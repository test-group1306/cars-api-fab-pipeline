package com.codewithz.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codewithz.model.FleetAge;
import com.codewithz.services.FleetsStatisticsService;

@RestController
@RequestMapping("statistics")
public class StatisticsController {

    @Autowired
    FleetsStatisticsService service;

    @GetMapping(value = "/{age}")
    public FleetAge getCarById() {
        return service.getAverageFleetAge();
    }
}

