package com.codewithz.services;

import java.util.List;
import java.util.OptionalDouble;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codewithz.model.Car;
import com.codewithz.model.FleetAge;
import com.codewithz.repository.CarsRepository;

@Service
public class FleetsStatisticsService {

    @Autowired
    private CarsRepository repository;

    public FleetAge getAverageFleetAge() {

        List<Car> cars = repository.findAll();

        OptionalDouble average = cars
                .stream()
                .mapToDouble(a -> (2020 - a.getBuild()))
                .average();

        return new FleetAge(cars.size(), average.getAsDouble());
    }
}
